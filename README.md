# FindIT4ALL

A simple proof of concept showing the user interface of the project developed during the Hackaton #euvsvirus## Getting Started

* [FindIT4ALL](https://www.youtube.com/watch?v=Ii0-4GS_jMU) - Project description
* [UserInterface](https://www.youtube.com/watch?v=2DvyV65TGiM) - User Interface explanation

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

In order to have this code up and running you need  to use python>3.6 and a instance of Prosody XMPP server

### Installing

Configure Prosodys

```
sudo mv prosody.cfg.lua /etc/prosody
sudo systemctl start prosody.service
```

Install requirements

```
pip install -r requirements.txt
```



## Built With

* [SPADE](https://github.com/javipalanca/spade) - The framework used
