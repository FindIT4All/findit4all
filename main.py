#!/usr/bin/env python3
"""
Simple proof of concept for the project FindIT4all

:author: Angelo Cutaia
:email: angelo.cutaia@linksfoundation.com
:copyright: Copyright 2020, Angelo Cutaia

..

    Copyright 2020 Angelo Cutaia

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

# Standard Libraries
import asyncio
import random
import time

# ThirdParty Libraries
import aioxmpp
import click
from aioxmpp import PresenceType, Presence, JID, PresenceShow, MessageType
from aioxmpp.roster.xso import Item
from spade import agent, behaviour
from spade.message import Message
from spade.template import Template


class MarketAgent(agent.Agent):
    """
    Fake Agents that rappresents the idea of
    the application FindIT4all
    """
    mask_request = None
    mask_offer = None
    best_offer = None

    class MedicalMaskRequest(behaviour.PeriodicBehaviour):
        """
        Fake Behaviour of the agent when it will receive a Medical Mask request
        """
        async def run(self):
            await asyncio.sleep(1)

    class MedicalMaskOffer(behaviour.PeriodicBehaviour):
        """
        Fake Behaviour of the agent when it will receive a Medical Mask offer
        """
        async def run(self):
            await asyncio.sleep(1)

    def setup(self):
        """
        Setup the fake Agent
        """
        # Create a Fake template for both the Producer and the consumer
        template_request = Template(sender="Hospital@localhost")
        template_offer = Template(sender="Third_Party_Company@localhost")

        # Create dummmy Request Behaviour
        self.mask_request = self.MedicalMaskRequest(period=10)
        self.add_behaviour(self.mask_request, template=template_request)

        # Create dummy Offer Behaviour
        self.mask_offer = self.MedicalMaskOffer(period=10)
        self.add_behaviour(self.mask_offer, template=template_offer)

        # Create some fake contacts
        self.add_fake_contact("Hospital@localhost", PresenceType.AVAILABLE, show=PresenceShow.CHAT)
        self.add_fake_contact("Pharmacy@localhost", PresenceType.UNAVAILABLE, show=PresenceShow.AWAY)
        self.add_fake_contact("Third_Party_Company@localhost", PresenceType.AVAILABLE, show=PresenceShow.CHAT)
        self.add_fake_contact("Common_Client@localhost", PresenceType.UNAVAILABLE, show=PresenceShow.AWAY)

        # Send and Receive some fake messages
        self.traces.reset()
        for i in range(10):
            self.fake_request_offer("Third_Party_Company")

    def fake_request_offer(self, company):
        """
        Generate Fake Medical Masks Request and Offer,
        just as a proof of concept of the application FindIT4All

        :param company: Company that is making the best offer
        """
        # Generate a random number of Masks required and a fake best offer
        number = random.randrange(100, 10000, 100)
        self.best_offer = random.randrange(number, number + 100, 1)

        # Fake message received from the Hospital fake agent
        from_ = JID.fromstr("Hospital@localhost")
        msg = aioxmpp.Message(from_=from_, to=self.jid, type_=MessageType.CHAT)
        msg.body[None] = "I need {} Medical Masks".format(number)
        msg = Message.from_node(msg)
        msg.metadata = {"performative": "inform", "acl-representation": "xml"}
        msg = msg.prepare()

        # Ask time to do analityc calculation in order to find the best offer
        self._message_received(msg=msg)
        msg = Message(sender=str(self.jid), to=str(from_), body="Give me some time to do analytic calculations")
        msg.sent = True

        # Wait for a fake offer
        self.fake_offer(number)

        # Generate the fake result and send it to the Hospital
        self.traces.append(msg, category=str(self.mask_request))
        msg = Message(
            sender=str(self.jid), to=str(from_), body=f"Best offert to buy {number} Medical Masks: {self.best_offer}$ "
                                                      f"made by {company}@localhost"
        )
        msg.sent = True
        self.traces.append(msg, category=str(self.mask_request))

    def fake_offer(self, number):
        """
        Generate a Fake offer for the number of Medical Masks asked by the hospital

        :param number: Medical Mask's nu,ber
        """
        # Generate Fake offer
        from_ = JID.fromstr("Third_Party_Company@localhost")
        msg = aioxmpp.Message(from_=from_, to=self.jid, type_=MessageType.CHAT)
        msg.body[None] = f"I offer {number} Medical Masks at {self.best_offer} $"
        msg = Message.from_node(msg)
        msg.metadata = {"performative": "inform", "acl-representation": "xml"}
        msg = msg.prepare()

        # Answer to the fake offer
        self._message_received(msg=msg)
        msg = Message(sender=str(self.jid), to=str(from_), body="Offer received, give me some time to find a buyer")
        msg.sent = True
        self.traces.append(msg, category=str(self.mask_offer))

    def add_fake_contact(self, jid, presence, show=None):
        """
        Add fake agent as contact in the contact's list

        :param jid: Agent's name plus domanin : agents@domain
        :param presence: Availability of the agent
        :param show: The type of Availability of the agent
        :return:
        """
        # Contact info
        jid = JID.fromstr(jid)
        item = Item(jid=jid)
        item.approved = True

        # Add the item to the presence roster
        self.presence.roster._update_entry(item)

        if show:
            stanza = Presence(from_=jid, type_=presence, show=show)
        else:
            stanza = Presence(from_=jid, type_=presence)
        self.presence.presenceclient.handle_presence(stanza)


@click.command()
@click.option('--provider', prompt="Mask Market> ")
@click.option('--pwd', prompt="Password>", hide_input=True)
@click.option('--port', default=10000)
def run(provider, pwd, port):
    """
    Start the Proof of concept application

    :param provider: Name of the Market agent
    :param pwd: password
    :param port: port used
    :return:
    """
    # Initialize
    host = f"{provider}@localhost"
    a = MarketAgent(host, pwd)
    a.start(auto_register=True)
    a.web.start(hostname="127.0.0.1", port=port)
    print(f"{provider} at {a.web.hostname}:{a.web.port}")

    # Wait for stop
    while a.is_alive():
        try:
            time.sleep(3)
        except KeyboardInterrupt:
            a.stop()


if __name__ == "__main__":
    run()
